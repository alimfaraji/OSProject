#include<stdio.h>
#include<unistd.h>
#include<sys/syscall.h>
#include<sys/time.h>

int main(){
	struct timeval t;
	long int secs;
	int hour, min, sec;
	int ret = syscall(SYS_gettimeofday, &t);
	if (ret == -1)
		printf("error");
	else{
		secs = (long int)t.tv_sec;
		secs += 3600 * 3 + 3600/2;
		hour = (secs%86400)/3600;
		min = (secs%3600)/60;
		sec = (secs%60);
		printf("%02d:%02d:%02d",hour, min, sec);
		
	}
}
