#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main ()
{

    long int filedesc = syscall(SYS_open, "/proc/stat", O_RDONLY);
    char c[2048];
    syscall(SYS_read, filedesc,&c,2048);
    syscall(SYS_close, filedesc);
    int u1, u2, u3, idle, u5, u6, u7, u8, u9, u10;
    sscanf(c, "%*s%d%d%d%d%d%d%d%d%d%d", &u1, &u2, &u3, &idle, &u5, &u6, &u7, &u8, &u9, &u10);


    printf("%f", 100.0 * (u1+u2+u3+u5+u6+u7+u8+u9+u10)/(u1+u2+u3+u5+u6+u7+u8+u9+u10+idle));
    return 0;
}