#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/delay.h>

int allocated[100000];
struct task_struct* usage_thread;
bool end = false;

int memory_usage(void){
  struct task_struct* proc;
  while(!end){
    ssleep(5);
    for_each_process(proc){
    if(proc->mm != NULL){
      allocated[proc->pid] = (proc->mm->total_vm - allocated[proc->pid]);
      printk(KERN_INFO "****** memory allocated by %d in 5 time units : %d ******", proc->pid, allocated[proc->pid]);
    }
    }
  }
  
  
}

int init_module(void){
  printk(KERN_INFO "INITIALIZED");
  printk(KERN_INFO "********************************************");
  struct task_struct* proc;
  for_each_process(proc){
  if(proc->mm !=  NULL){
    allocated[proc->pid] = proc->mm->total_vm;
    printk(KERN_INFO "****** memory allocated by %d at starting : %d ******", proc->pid, allocated[proc->pid]);
  }
  }
  usage_thread = kthread_create(memory_usage, NULL, "USAGE");
  usage_thread = kthread_run(memory_usage, NULL, "USAGE");
  return 0;
}

void cleanup_module(void){
  printk(KERN_INFO "********************************************");
  printk(KERN_INFO "ENDED");
  end = true;
}

